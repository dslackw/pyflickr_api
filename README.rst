.. contents:: Table of Contents:


About
-----

The `Flickr API <https://www.flickr.com/services/api/misc.overview.html>`_ consists of a set of callable methods, 
and some API endpoints.

To perform an action using the Flickr API, you need to select a calling convention, 
send a request to its endpoint specifying a method and some arguments, and will receive a formatted response.

Response
--------

Starts by creating a Flickr API object with your API key and secret.

.. code-block:: bash

    rest_url = 'https://api.flickr.com/services/rest/'

    flickr = FlickrAPI(api_key, secret_key, rest_url, user_id=user_id, format='json')
    query = flickr.photos.search()
    json = flickr.get(query)

    {
    "photos": {
        "page": 1,
        "pages": 1,
        "perpage": 100,
        "total": 1,
        "photo": [
        {
            "id": "80692123230003",
            "owner": "198653127@N07",
            "secret": "410b7a21c4",
            "server": "65535",
            "farm": 66,
            "title": "hello",
            "ispublic": 1,
            "isfriend": 0,
            "isfamily": 0
        }
        ]
    },
    "stat": "ok"
    }

* `The App Garden <https://www.flickr.com/services/>`_
* `API Documentation <https://www.flickr.com/services/api/>`_
* `Create an App <https://www.flickr.com/services/apps/create/>`_
 


