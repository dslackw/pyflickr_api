#!/usr/bin/env python
# -*- coding: utf-8 -*-


class CallMaker:
    """Create a method name for FlickrAPI
    """

    def __init__(self, flickrAPI_obj, method_name='flickr'):
        self.method_name = method_name
        self.flickrAPI_obj = flickrAPI_obj

    def __getattr__(self, name):
        return self.flickrAPI_obj(self.flickrAPI_obj,
                                  f'{self.method_name}.{name}')

    def __repr__(self):
        return self.method_name

    def __call__(self, **kwargs):
        return self.method_name, kwargs
