#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import requests
from dotenv import load_dotenv, find_dotenv
from call_maker import CallMaker


# Load values from .env file
load_dotenv(find_dotenv())


class FlickrAPI:

    def __init__(self, api_key, secret_key, rest_url, user_id='',
                 format=''):
        self.api_key = api_key
        self.secret_key = secret_key
        self.rest_url = rest_url
        self.user_id = user_id
        self.format = format
        self.nojsoncallback = ''

        json = {'json': '1',
                'jsonp': '0',
                '': ''}
        try:
            self.nojsoncallback = json.get(self.format)
        except KeyError:
            raise KeyError('Unknow format')

        if self.nojsoncallback == '0':
            self.format = 'json'

    def __getattr__(self, method_name):
        return CallMaker(CallMaker, method_name)

    def __call__(self, *args, **kwargs):
        return args, kwargs

    def parameters(self, query):
        """Create a dictionary with all parameters
        """
        params = {}
        args = query[1]

        dct = {
            'api_key': self.api_key,
            'user_id': self.user_id,
            'method': f'flickr.{query[0]}',
            'format': self.format,
            'nojsoncallback': self.nojsoncallback
        }

        dct.update(args)

        for k, v in dct.items():
            params.update({k: v})

        return params

    def get(self, query):
        r = requests.get(self.rest_url, params=self.parameters(query))
        return r.text

    def post(self, query):
        r = requests.post(self.rest_url, params=self.parameters(query))
        return r.text


"""
api_key = os.getenv('API_KEY')
secret_key = os.getenv('SECRET_KEY')
user_id = os.getenv('USER_ID')

rest_url = 'https://api.flickr.com/services/rest/'

flickr = FlickrAPI(api_key, secret_key, rest_url, user_id=user_id,
                   format='json')
query = flickr.photos.search()
json = flickr.get(query)
"""
